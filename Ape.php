<?php
class Ape extends Animal {
    public function __construct() {
        $this->legs = 2;
    }

    public function yell() {
        echo "Auooo";
    }
}
?>