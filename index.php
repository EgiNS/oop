<?php
require ('animal.php');
require ('Frog.php');
require ('Ape.php');

$sheep = new Animal("shaun");
echo "Name : $sheep->name"; // "shaun"
echo "<br>";
echo "legs : $sheep->legs"; // 4
echo "<br>";
echo "cold blooded : $sheep->cold_blooded"; // "no"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : $kodok->name"; 
echo "<br>";
echo "legs : $kodok->legs";
echo "<br>";
echo "cold blooded : $kodok->cold_blooded";
echo "<br>";
echo "Jump : ";
$kodok->jump() ; // "hop hop
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name"; 
echo "<br>";
echo "legs : $sungokong->legs";
echo "<br>";
echo "cold blooded : $sungokong->cold_blooded";
echo "Yell : ";
$sungokong->yell(); // "Auooo"

?>